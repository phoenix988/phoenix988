# Karl Fredin

# About Me

Welcome to my profile!

![Profile Banner](./images/git-profile-banner.png)

## My Repositories
Some of my repositores I maintain on git lab.

| Repositories                                                     | Usage                          |
| -------------------------------------------------------------- | ------------------------------ |
| [_**Azla**_](https://gitlab.com/phoenix988/azla)                     | Language learning              |
| [_**Awesome**_](https://gitlab.com/phoenix988/awesome)               | Awesome  WM         |
| [_**Dmscripts**_](https://gitlab.com/phoenix988/dmscripts)           | Collection of my rofi scripts        |
| [_**Dotfiles**_](https://gitlab.com/phoenix988/dotfiles)             | Manage my config files         |
| [_**Fonts**_](https://gitlab.com/phoenix988/fonts)                   | Collection of fonts            |
| [_**Hyprland**_](https://gitlab.com/phoenix988/hypr)                 | Hyprland WM        |
| [_**Neovim**_](https://gitlab.com/phoenix988/dotfiles/-/tree/neovim) | Neovim Text editor                  |
| [_**Setup**_](https://gitlab.com/phoenix988/setup)                   | Automation setup script              |
| [_**Slock-Phoenix**_](https://github.com/phoenix988/slock-phoenix)   | Normal slock with some patches |
| [_**Qtile**_](https://gitlab.com/phoenix988/dotfiles/-/tree/qtile)   | Qtile WM           |
| [_**Wallpapers**_](https://gitlab.com/phoenix988/wallpapers)         | Collection of wallpapers       |

## Softwares
The softwares I currently use or have used in the past

### Window Managers:

- _Qtile_
- _Hyprland_ (__*Main__)
- _Awesome_

#
### Showcase


![Hyprland Desktop](./images/hyprland-desktop-nord.jpeg)
_**Hyprland**_

![Qtile](./images/qtile.png)
_**Qtile**_

### Text Editors
Text editors I used. I do prefer neovim over emacs but if I ever need to work with org documents
That is when I use emacs because org mode in emacs is way superior.

- _Doom Emacs_
- _Neovim_ (__Main__)

![Neovim](./images/nvim-doom.png)
_**Neovim**_

### Other Tools
Some other tools I use. For video editing, Image editing etc.

- _Kdenlive_
- _Gimp_
